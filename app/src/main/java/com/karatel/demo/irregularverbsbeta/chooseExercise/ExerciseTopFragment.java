package com.karatel.demo.irregularverbsbeta.chooseExercise;

import android.os.Bundle;
import android.support.v4.app.*;
import android.view.*;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment;
import com.karatel.demo.irregularverbsbeta.recyclers.ChooseGroupRecycler;
import com.karatel.demo.irregularverbsbeta.studyActivities.RepeatActivity;

import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.GET_VERBS_FOR_REPEAT;

public class ExerciseTopFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            Fragment lowerFragment;
            if (RepeatActivity.class.isAssignableFrom(getActivity().getClass())) {
                TasksManagerFragment.prepare().task(GET_VERBS_FOR_REPEAT).
                        startTask(getFragmentManager());
                lowerFragment = new ChooseRepeatModeFragment();
            } else
                lowerFragment = new ChooseGroupRecycler();
            ft.add(R.id.lower_fragment_container, lowerFragment).commit();
        }
        return inflater.inflate(R.layout.fragment_exercises_top, container, false);
    }
}
