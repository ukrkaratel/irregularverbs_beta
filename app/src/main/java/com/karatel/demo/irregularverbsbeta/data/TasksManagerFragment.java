package com.karatel.demo.irregularverbsbeta.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.*;
import android.util.Log;

import com.karatel.demo.irregularverbsbeta.R;

import java.util.ArrayList;

import static com.karatel.demo.irregularverbsbeta.data.Status.receiveStatus;

public class TasksManagerFragment extends Fragment {
    public static final String GET_LEVELS_TITLES = "get_levels_titles";
    public static final String GET_VERBS_BY_LEVEL = "get_by_level";
    public static final String GET_VERBS_BY_INFINITIVES = "get_by_infinitives";
    public static final String SAVE_VERBS_STATUS = "save_verbs_status";
    public static final String SETUP_PIE_CHART = "setup_pie_chart";
    public static final String GET_VERBS_FOR_REPEAT = "get_for_repeat";
    public static final String GET_ALL_VERBS = "fill_table";
    private static final String STRING_ARRAY = "string_array";
    private static final String WHOLE_NUMBER = "whole_number";

    public interface TaskCallbacks {
        void onPostExecute(Object result);
    }

    private Object mCallbacks;
    private String typeOfTask;

    public static TasksManagerFragment prepare() {
        TasksManagerFragment taskManFragment = new TasksManagerFragment();
        taskManFragment.setArguments(new Bundle());
        return taskManFragment;
    }

    public static void finishTask(FragmentManager fm, String typeOfTask) {
        Fragment taskFragment = fm.findFragmentByTag(typeOfTask);
        fm.beginTransaction().remove(taskFragment).commit();
    }

    public TasksManagerFragment task(String typeOfTask) {
        this.typeOfTask = typeOfTask;
        return this;
    }

    public TasksManagerFragment param(int wholeNumber) {
        getArguments().putInt(WHOLE_NUMBER, wholeNumber);
        return this;
    }

    public TasksManagerFragment param(String[] strings) {
        getArguments().putStringArray(STRING_ARRAY, strings);
        return this;
    }

    public TasksManagerFragment callback(TaskCallbacks callbacks) {
        mCallbacks = callbacks;
        return this;
    }

    public void startTask(FragmentManager fm) {
        fm.beginTransaction().add(this, typeOfTask).commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (mCallbacks == null)
            mCallbacks = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        switch (typeOfTask) {
            default:
            case GET_LEVELS_TITLES:
                new GetLevelsTitlesTask().execute(getArguments().getInt(WHOLE_NUMBER));
                break;
            case SETUP_PIE_CHART:
                new SetupPieChartTask().execute();
                break;
            case GET_VERBS_BY_LEVEL:
                new GetVerbsTask().execute(getArguments().getInt(WHOLE_NUMBER));
                break;
            case SAVE_VERBS_STATUS:
                new SaveVerbsStatusTask().execute(getArguments().getStringArray(STRING_ARRAY));
                break;
            case GET_VERBS_FOR_REPEAT:
            case GET_ALL_VERBS:
                new GetVerbsTask().execute();
                break;
            case GET_VERBS_BY_INFINITIVES:
                new GetVerbsTask().execute((Object[]) getArguments().getStringArray(STRING_ARRAY));
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    private class GetLevelsTitlesTask extends AsyncTask<Integer, Void, Boolean> {
        private final static int UNCHANGEABLE = 0;
        private final static int TWO_FORMS_SAME = 1;
        private final static int ALL_FORMS_DIFFERENT = 2;

        private final static int LEVEL = 0;
        private final static int INFINITIVE = 1;
        private final static int PAST_SIMPLE = 2;
        private final static int PAST_PARTICIPLE = 3;

        private String[] titlesTemp;

        @Override
        protected Boolean doInBackground(Integer... integers) {
            int verbsGroup = integers[0];
            StringBuilder stringBuilder = new StringBuilder();
            try (SQLiteDatabase db = new IrrVerbsDatabaseHelper(getActivity())
                    .getReadableDatabase()) {
                int lowerLimit, upperLimit;
                switch (verbsGroup) {
                    default:
                    case UNCHANGEABLE:
                        lowerLimit = 0;
                        upperLimit = getUpperLimit("unchangeable", 1);
                        break;
                    case TWO_FORMS_SAME:
                        lowerLimit = 5;
                        upperLimit = getUpperLimit("two_forms_same", 6);
                        break;
                    case ALL_FORMS_DIFFERENT:
                        lowerLimit = 27;
                        upperLimit = getUpperLimit("all_forms_different", 28);
                }
                Cursor cursor = db.query("TEST",
                        new String[]{"LEVEL", "INFINITIVE", "PAST_SIMPLE", "PAST_PARTICIPLE"},
                        "LEVEL > " + lowerLimit + " AND LEVEL <= " + upperLimit,
                        null, null, null, null);
                titlesTemp = new String[upperLimit - lowerLimit];
                int level = ++lowerLimit, index = titlesTemp.length - 1;
                stringBuilder.append((char) level);
                while (cursor.moveToNext()) {
                    if (cursor.getInt(LEVEL) != level) {
                        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
                        titlesTemp[index--] = stringBuilder.toString();
                        stringBuilder.setLength(0);
                        stringBuilder.append((char) ++level);
                    }
                    stringBuilder.append(cursor.getString(INFINITIVE)).append(" - ")
                            .append(cursor.getString(PAST_SIMPLE)).append(" - ")
                            .append(cursor.getString(PAST_PARTICIPLE)).append("\n\n");
                }
                stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
                titlesTemp[index] = stringBuilder.toString();
                cursor.close();
                return true;
            } catch (SQLiteException e) {
                Log.e("ERROR IN TaskManager", "Database is unavailable: " + e.getMessage());
                return false;
            }
        }

        private int getUpperLimit(String groupType, int firstLevel) {
            return getActivity().getSharedPreferences("open_levels", Context.MODE_PRIVATE)
                    .getInt(groupType, firstLevel);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            onPostExec(success, titlesTemp);
        }
    }

    private class GetVerbsTask extends AsyncTask<Object, Void, Boolean> {
        private String[] translations;
        private ArrayList<Verb> verbsListTemp;

        @Override
        protected void onPreExecute() {
            translations = getResources().getStringArray(R.array.translations);
            verbsListTemp = new ArrayList<>();
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            try (SQLiteDatabase db = new IrrVerbsDatabaseHelper(getActivity()).
                    getReadableDatabase()) {
                String[] columns = new String[]{"INFINITIVE", "PAST_SIMPLE", "PAST_PARTICIPLE",
                        "_id", "STATUS"};
                String selection = null;
                String[] selectionArgs = null;
                String orderBy = null;
                switch (typeOfTask) {
                    case GET_VERBS_BY_LEVEL:
                        selection = "LEVEL = ?";
                        selectionArgs = new String[]{String.valueOf(params[0])};
                        break;
                    case GET_VERBS_BY_INFINITIVES:
                        String[] infinitives = (String[]) params;
                        selection = getVerbsByInfinitivesSelection(infinitives);
                        selectionArgs = infinitives;
                        break;
                    case GET_VERBS_FOR_REPEAT:
                        selection = "STATUS > 0";
                        break;
                    case GET_ALL_VERBS:
                        columns[4] = "VERB_GROUP";
                        selection = null;
                        orderBy = "INFINITIVE ASC";
                }
                Cursor cursor = db.query("TEST", columns, selection, selectionArgs,
                        null, null, orderBy);
                while (cursor.moveToNext()) {
                    Verb.VerbBuilder verbBuilder = Verb.builder()
                            .infinitive(cursor.getString(0)).pastSimple(cursor.getString(1)).
                                    pastParticiple(cursor.getString(2))
                            .translation(translations[cursor.getInt(3) - 1]);
                    if (typeOfTask.equals(GET_ALL_VERBS))
                        verbBuilder.groupNum(cursor.getInt(4));
                    else
                        verbBuilder.status(cursor.getInt(4));
                    verbsListTemp.add(verbBuilder.build());
                }
                cursor.close();
                return true;
            } catch (SQLiteException e) {
                Log.e("ERROR IN TaskManager", "Database is unavailable: " + e.getMessage());
                return false;
            }
        }

        private String getVerbsByInfinitivesSelection(String[] infinitives) {
            StringBuilder selection = new StringBuilder("INFINITIVE IN(");
            for (int i = 0; i < infinitives.length; i++)
                selection.append("?,");
            selection.setCharAt(selection.length() - 1, ')');
            return selection.toString();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            Log.d("MARK", "DONE");
            onPostExec(success, verbsListTemp);
        }
    }

    private class SaveVerbsStatusTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... verbStatusArray) {
            try (SQLiteDatabase db =
                         new IrrVerbsDatabaseHelper(getActivity()).getWritableDatabase()) {
                StringBuilder infinitives = new StringBuilder("(");
                String sqlStr = "UPDATE TEST " +
                        "SET STATUS = CASE INFINITIVE ";
                for (String verbStatusStr : verbStatusArray) {
                    Log.d("SAVE STATUS", verbStatusStr);
                    String[] infinitiveAndStatus = verbStatusStr.split(" ");
                    sqlStr += "WHEN '" + infinitiveAndStatus[0] + "' THEN '" +
                            infinitiveAndStatus[1] + "' ";
                    infinitives.append("'").
                            append(infinitiveAndStatus[0]).
                            append("', ");
                }
                infinitives.delete(infinitives.length() - 2, infinitives.length());
                sqlStr += "ELSE STATUS " +
                        "END " +
                        "WHERE INFINITIVE IN" + infinitives.toString() + ");";
                db.execSQL(sqlStr);
                db.close();
            } catch (SQLiteException e) {
                Log.e("ERROR IN TaskManager", "Database is unavailable: " + e.getMessage());
            }
            return null;
        }
    }

    private class SetupPieChartTask extends AsyncTask<Void, Void, Boolean> {
        private static final int STATUS = 0;
        private static final int COUNT = 1;
        private static final int WILL_LEARN_STATUS_IND = 0;
        private static final int LEARNING_STATUS_IND = 1;
        private static final int HAVE_LEARNED_STATUS_IND = 2;
        private int[] pieChartParamsTemp;

        @Override
        protected void onPreExecute() {
            pieChartParamsTemp = new int[3];
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try (SQLiteDatabase db = new IrrVerbsDatabaseHelper(getActivity())
                    .getReadableDatabase()) {
                Cursor cursor = db.query("TEST",
                        new String[]{"STATUS", "COUNT(_id) AS count"},
                        null, null,
                        "STATUS",
                        null, null);
                while (cursor.moveToNext())
                    switch (receiveStatus(cursor.getInt(STATUS))) {
                        case WILL_LEARN:
                            pieChartParamsTemp[WILL_LEARN_STATUS_IND] = cursor.getInt(COUNT);
                            break;
                        case LEARNING:
                            increaseCounter(LEARNING_STATUS_IND, cursor.getInt(COUNT));
                            break;
                        case HAVE_LEARNED:
                            increaseCounter(HAVE_LEARNED_STATUS_IND, cursor.getInt(COUNT));
                    }
                cursor.close();
                return true;
            } catch (SQLiteException e) {
                Log.e("ERROR IN TaskManager", "Database is unavailable: " + e.getMessage());
                return false;
            }
        }

        private void increaseCounter(int index, int newCount) {
            int currentCount = pieChartParamsTemp[index];
            pieChartParamsTemp[index] = currentCount == 0 ? newCount : (newCount + currentCount);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            onPostExec(success, pieChartParamsTemp);
        }
    }

    private void onPostExec(boolean success, Object result) {
        if (success && mCallbacks instanceof TaskCallbacks)
            ((TaskCallbacks) mCallbacks).onPostExecute(result);
    }

}
