package com.karatel.demo.irregularverbsbeta.data;

/*
* Status is a scale from 0 to 7 (STATUS_UPPER_LIMIT). There are three statuses' ranks and
* each of them has a lower limit */
public enum Status {
    WILL_LEARN(0), LEARNING(1), HAVE_LEARNED(5);

    private static final int STATUS_UPPER_LIMIT = 7;

    private final int lowerLimit;

    Status(int lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    /*This method is called, when user makes an answer. Status cannot be less than lower limit and
    * higher than upper limit. Also verb cannot have a status 'WILL_LEARN', because user
    * are learning it*/
    public static int correct(int newStatus) {
        if (newStatus < WILL_LEARN.lowerLimit)
            return WILL_LEARN.lowerLimit;
        else if (newStatus == WILL_LEARN.lowerLimit)
            return LEARNING.lowerLimit;
        else if (newStatus > STATUS_UPPER_LIMIT)
            return STATUS_UPPER_LIMIT;
        else
            return newStatus;
    }

    static Status receiveStatus(int statusNum) {
        if (statusNum == WILL_LEARN.lowerLimit)
            return WILL_LEARN;
        else if (statusNum >= LEARNING.lowerLimit &&
                statusNum < HAVE_LEARNED.lowerLimit)
            return LEARNING;
        else
            return HAVE_LEARNED;
    }
}
