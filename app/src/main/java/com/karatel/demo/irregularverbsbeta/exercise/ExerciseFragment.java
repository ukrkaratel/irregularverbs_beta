package com.karatel.demo.irregularverbsbeta.exercise;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.*;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.view.*;
import android.widget.FrameLayout;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.Status;
import com.karatel.demo.irregularverbsbeta.data.Verb;
import com.karatel.demo.irregularverbsbeta.studyActivities.*;

import java.util.*;

import at.markushi.ui.CircleButton;

public class ExerciseFragment extends Fragment implements Refreshable {
    public static int[] fieldsIDs;
    private static Random random;

    static {
        fieldsIDs = new int[]{R.id.infinitive_container, R.id.past_simple_container,
                R.id.past_participle_container, R.id.translation_container};
        random = new Random();
    }

    private CanExercise canExercise;
    private String[] verbsFormsNames;
    private int currentVerbNum;
    private Verb currentVerb;
    private ArrayList<ExerciseFieldFragment> fields;
    private CircleButton checkContinueBtn;
    private boolean isContinueButton;
    private boolean isCorrectAnswer;
    private boolean btnInterferesWithText;
    private ArrayMap<String, Integer> verbsStatus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CanExercise))
            throw new RuntimeException(context.toString()
                    + " must implement CanExercise");
        else
            canExercise = (CanExercise) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_exercise, container, false);
        checkContinueBtn = layout.findViewById(R.id.check_continue_btn);
        checkContinueBtn.setOnClickListener(new CheckContinueOnClick());
        if (savedInstanceState == null) {
            initVerbsFormsNamesArray();
            fields = new ArrayList<>();
            verbsStatus = new ArrayMap<>();
            createFields();
        } else {
            isContinueButton = savedInstanceState.getBoolean("is_continue_btn");
            reestablishFields();
            verbsFormsNames = savedInstanceState.getStringArray("verbs_forms_names");
            currentVerbNum = savedInstanceState.getInt("verb_num");
            isCorrectAnswer = savedInstanceState.getBoolean("is_correct_answer");
            reestablishVerbsStatus(savedInstanceState.getStringArrayList("progress"));
            checkBtnLocation(savedInstanceState.getBoolean("btn_interferes"));
            if (canExercise.getVerbsListSize() != 0) {
                refresh();
            }
        }
        return layout;
    }

    private void initVerbsFormsNamesArray() {
        String translation = getString(R.string.translation);
        verbsFormsNames = new String[]{"infinitive", "past simple", "past participle", translation};
    }

    @Override
    public void refresh() {
        if (checkContinueBtn == null) return;
        currentVerb = canExercise.getVerb(currentVerbNum);
    }

    private void createFields() {
        currentVerbNum = random.nextInt(canExercise.getVerbsListSize());
        refresh();
        String[] currentVerbsForms =
                new String[]{currentVerb.getInfinitive(), currentVerb.getPastSimple(),
                        currentVerb.getPastParticiple(), currentVerb.getTranslation()};
        int openFieldNum = random.nextInt(4);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ExerciseFieldFragment fieldFragment;
        for (int i = 0; i < 4; i++) {
            fieldFragment = i == openFieldNum ?
                    ExerciseFieldFragment.
                            newInstance(true, currentVerbsForms[i]) :
                    ExerciseFieldFragment.
                            newInstance(false, currentVerbsForms[i], verbsFormsNames[i]);
            fields.add(fieldFragment);
            ft.replace(fieldsIDs[i], fieldFragment, "mark" + i);
        }
        ft.commit();
    }

    private class CheckContinueOnClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (currentVerb == null) return;
            if (isContinueButton) {
                if (isCorrectAnswer && canExercise.removeVerb(currentVerb))
                    canExercise.saveAndShowProgress(verbsStatus);
                else {
                    if (btnInterferesWithText)
                        moveBtn(Gravity.END, ContextCompat.getColor(getActivity(), R.color.yellow));
                    reset();
                    createFields();
                }
            } else {
                checkAnswers();
                updateVerbsStatus();
                checkBtnLocation(!isCorrectAnswer &&
                        fields.get(3).isIncorrect());
                isContinueButton = true;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArray("verbs_forms_names", verbsFormsNames);
        outState.putStringArrayList("progress", saveProgress());
        outState.putInt("verb_num", currentVerbNum);
        outState.putBoolean("is_continue_btn", isContinueButton);
        outState.putBoolean("is_correct_answer", isCorrectAnswer);
        outState.putBoolean("btn_interferes", btnInterferesWithText);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        canExercise = null;
    }

    private void reestablishFields() {
        fields = new ArrayList<>();
        if (isContinueButton) return;
        for (int i = 0; i < 4; i++)
            fields.add((ExerciseFieldFragment) getChildFragmentManager().
                    findFragmentByTag("mark" + i));
    }

    private void updateVerbsStatus() {
        Integer status = verbsStatus.get(currentVerb.getInfinitive());
        if (status == null)
            status = currentVerb.getStatus();
        verbsStatus.put(currentVerb.getInfinitive(),
                Status.correct(status + (isCorrectAnswer ? 1 : -1)));
    }

    private void checkAnswers() {
        isCorrectAnswer = true;
        for (ExerciseFieldFragment fieldFragment : fields)
            if (!fieldFragment.makeAnswer())
                isCorrectAnswer = false;
    }

    private void checkBtnLocation(boolean btnInterferesWithText) {
        if (this.btnInterferesWithText = btnInterferesWithText)
            moveBtn(Gravity.CENTER, Color.TRANSPARENT);
    }

    private void moveBtn(int gravity, @ColorInt int color) {
        ((FrameLayout.LayoutParams) checkContinueBtn.getLayoutParams()).gravity
                = Gravity.BOTTOM | gravity;
        checkContinueBtn.setColor(color);
    }

    private void reset() {
        isContinueButton = false;
        isCorrectAnswer = false;
        btnInterferesWithText = false;
        fields.clear();
    }

    private ArrayList<String> saveProgress() {
        ArrayList<String> verbsStatusList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : verbsStatus.entrySet()) {
            verbsStatusList.add(entry.getKey());
            verbsStatusList.add(entry.getValue().toString());
        }
        return verbsStatusList;
    }

    private void reestablishVerbsStatus(ArrayList<String> verbsStatusList) {
        verbsStatus = new ArrayMap<>();
        for (int i = 1; i < verbsStatusList.size(); i += 2)
            verbsStatus.put(verbsStatusList.get(i - 1), Integer.parseInt(verbsStatusList.get(i)));
    }
}
