package com.karatel.demo.irregularverbsbeta.exercise;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.karatel.demo.irregularverbsbeta.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ExerciseFieldFragment extends Fragment {
    private static final String FIELD_STATUS = "FIELD_STATUS";
    private static final String FIELD_TEXT = "FIELD_TEXT";
    private static final String VERB_STRING = "VERB_STRING";
    private static final int[] colors;
    private static int counter = -1;

    static {
        colors = new int[]{R.color.orange, R.color.yellow, R.color.green1,
                R.color.light_blue};
    }

    private boolean isOpen;
    private String fieldText;
    private TextView fieldView;
    private String verbStr;
    private int index;
    private boolean isCorrect;
    private boolean isIncorrect;

    public boolean isIncorrect() {
        return isIncorrect;
    }

    public static ExerciseFieldFragment newInstance(boolean isOpen, String verbStr) {
        ExerciseFieldFragment exerciseFieldFragment = new ExerciseFieldFragment();
        Bundle args = new Bundle();
        args.putBoolean(FIELD_STATUS, isOpen);
        args.putString(VERB_STRING, verbStr);
        exerciseFieldFragment.setArguments(args);
        return exerciseFieldFragment;
    }

    public static ExerciseFieldFragment newInstance(boolean isOpen, String verbStr,
                                                    String fieldText) {
        ExerciseFieldFragment exerciseFieldFragment = newInstance(isOpen, verbStr);
        exerciseFieldFragment.getArguments().putString(FIELD_TEXT, fieldText);
        return exerciseFieldFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isOpen = getArguments().getBoolean(FIELD_STATUS);
            verbStr = getArguments().getString(VERB_STRING);
            fieldText = getArguments().getString(FIELD_TEXT, null);
        }
        if (savedInstanceState != null)
            index = savedInstanceState.getInt("index");
        else
            index = ++counter % 4;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(isOpen ?
                        R.layout.fragment_exercise_open_field :
                        R.layout.fragment_exercise_field,
                container, false);
        fieldView = layout.findViewById(R.id.field_txt);
        if (savedInstanceState != null) {
            isCorrect = savedInstanceState.getBoolean("is_correct");
            if (isCorrect) {
                makeCorrectAnswer();
                return layout;
            }
            isIncorrect = savedInstanceState.getBoolean("is_incorrect");
            if (isIncorrect) {
                fieldView.setKeyListener(null);
                return layout;
            }
        }
        int currentColor = ContextCompat.getColor(getActivity(), colors[index]);
        fieldView.setTextColor(currentColor);
        if (isOpen){
            fitOutText();
            return layout;
        }
        fieldView.setHint(fieldText);
        EditTextPainter.prepare((EditText) fieldView, currentColor).paint();
        return layout;
    }

    private static class EditTextPainter {
        private EditText editText;
        private int color;

        private EditTextPainter(EditText editText, int color) {
            this.editText = editText;
            this.color = color;
        }

        private static EditTextPainter prepare(@NonNull EditText editText, @ColorInt int color) {
            return new EditTextPainter(editText, color);
        }

        private void paint() {
            underline();
            cursorAndHandle();
        }

        private void underline() {
            editText.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }

        private void cursorAndHandle() {
            try {
                Field field = TextView.class.getDeclaredField("mEditor");
                field.setAccessible(true);
                Object editor = field.get(editText);
                cursor(editor);
                handles(editor);
            } catch (Exception ignore) {
            }
        }

        private void cursor(Object editor) throws Exception {
            GradientDrawable newCursorDrawable = new GradientDrawable();
            newCursorDrawable.setColor(color);
            newCursorDrawable.setSize(9, -1);
            Drawable[] drawables = {newCursorDrawable, newCursorDrawable};

            Field field = editor.getClass().getDeclaredField("mCursorDrawable");
            field.setAccessible(true);
            field.set(editor, drawables);
        }

        private void handles(Object editor) throws Exception {
            String[] resFieldNames = {"mTextSelectHandleLeftRes", "mTextSelectHandleRightRes",
                    "mTextSelectHandleRes"};
            String[] drawableFieldNames = {"mSelectHandleLeft", "mSelectHandleRight",
                    "mSelectHandleCenter"};

            for (int i = 0; i < resFieldNames.length; i++) {
                String resFieldName = resFieldNames[i];
                String drawableFieldName = drawableFieldNames[i];

                Field field = TextView.class.getDeclaredField(resFieldName);
                field.setAccessible(true);
                int selectHandleRes = field.getInt(editText);

                Drawable selectHandleDrawable = ContextCompat.getDrawable(editText.getContext(),
                        selectHandleRes).mutate();
                selectHandleDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);

                field = editor.getClass().getDeclaredField(drawableFieldName);
                field.setAccessible(true);
                field.set(editor, selectHandleDrawable);
            }
        }
    }

    private void openField() {
        fieldView.setEnabled(false);
        fieldView.setCursorVisible(false);
        fieldView.setBackgroundColor(Color.TRANSPARENT);
        fieldView.setKeyListener(null);
        fieldView.setGravity(Gravity.CENTER);
        fitOutText();
    }

    private void fitOutText() {
        if (verbStr.length() > 18) {
            verbStr = verbStr.replace(", ", ",\n");
            fieldView.setSingleLine(false);
        }
        fieldView.setText(verbStr);
    }

    public boolean makeAnswer() {
        if (isOpen) return true;
        String answer = fieldView.getText().toString();
        String answerStr = answer.trim().toLowerCase();
        if (check(answerStr)) {
            makeCorrectAnswer();
            return true;
        } else {
            makeIncorrectAnswer(answer);
            return false;
        }
    }

    private boolean check(String answerStr) {
        if (verbStr.contains(",") && answerStr.contains(",")) {
            String[] answerVariants = answerStr.split(",");
            List<String> correctVariants = new ArrayList<>(Arrays.asList(verbStr.split(", ")));
            boolean isCorrect;
            for (String answerVariant : answerVariants) {
                isCorrect = false;
                answerVariant = answerVariant.trim();
                for (String correctVariant : correctVariants)
                    if (answerVariant.equals(correctVariant)) {
                        isCorrect = true;
                        correctVariants.remove(correctVariant);
                        break;
                    }
                if (!isCorrect) return false;
            }
            return true;
        } else if (verbStr.contains(",")) {
            String[] correctVariants = verbStr.split(", ");
            for (String variant : correctVariants)
                if (variant.equals(answerStr))
                    return true;
            return false;
        } else
            return verbStr.equals(answerStr);

    }

    private void makeCorrectAnswer() {
        openField();
        fieldView.setTextColor(Color.WHITE);
        View container = getActivity().findViewById(ExerciseFragment.fieldsIDs[index]);
        container.setBackgroundColor(ContextCompat.getColor(fieldView.getContext(),
                R.color.green2));
        isCorrect = true;
    }

    private void makeIncorrectAnswer(String incAnswer) {
        fieldView.setKeyListener(null);
        isIncorrect = true;
        getParentFragment().getChildFragmentManager().beginTransaction().
                add(ExerciseFragment.fieldsIDs[index],
                        IncorrectAnswerFragment.newInstance(incAnswer, verbStr)).commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_correct", isCorrect);
        outState.putBoolean("is_incorrect", isIncorrect);
        outState.putInt("index", index);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (isCorrect)
            getActivity().findViewById(ExerciseFragment.fieldsIDs[index])
                    .setBackgroundColor(Color.BLACK);
    }
}
