package com.karatel.demo.irregularverbsbeta.data;

import lombok.Builder;
import lombok.Getter;

@Getter @Builder
public class Verb {
    private String infinitive;
    private String pastSimple;
    private String pastParticiple;
    private String translation;
    private int status;
    private int groupNum;
}
