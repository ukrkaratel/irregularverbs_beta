package com.karatel.demo.irregularverbsbeta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.karatel.demo.irregularverbsbeta.chooseExercise.ChooseExerciseActivity;
import com.karatel.demo.irregularverbsbeta.studyActivities.RepeatActivity;
import com.karatel.demo.irregularverbsbeta.tables.TablesActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupListView();
    }

    private void setupListView() {
        String[] options = getResources().getStringArray(R.array.options);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.my_custom_layout_main, options);
        ListView listView = (ListView) findViewById(R.id.list_options);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    default:
                    case 0:
                        beginActivity(ChooseExerciseActivity.class);
                        break;
                    case 1:
                        beginActivity(RepeatActivity.class);
//                        Toast.makeText(MainActivity.this, "It isn't realized yet :(" +
//                                position, Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        beginActivity(TablesActivity.class);
                }
            }

            private void beginActivity(Class activityClass) {
                startActivity(new Intent(MainActivity.this, activityClass));
            }
        });
    }

}
