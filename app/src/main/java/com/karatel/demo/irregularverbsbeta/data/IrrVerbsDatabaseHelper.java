package com.karatel.demo.irregularverbsbeta.data;

import android.content.*;
import android.database.sqlite.*;
import android.util.Log;

import com.karatel.demo.irregularverbsbeta.R;

import java.io.*;
import java.nio.charset.Charset;

class IrrVerbsDatabaseHelper extends SQLiteOpenHelper {

    private final static String DB_NAME = "test";
    private final static int DB_VERSION = 1;
    private Context context;

    IrrVerbsDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db, oldVersion, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("CREATE TABLE TEST (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "INFINITIVE TEXT, " +
                "PAST_SIMPLE TEXT, " +
                "PAST_PARTICIPLE TEXT, " +
                "LEVEL INTEGER, " +
                "VERB_GROUP INTEGER," +
                "STATUS INTEGER);");
        InputStream is = context.getResources().openRawResource(R.raw.data);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8")));
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(";");

                String infinitive = tokens[0];
                String past_Simple = tokens[1];
                String past_Participle = tokens[2];
                int level = Integer.parseInt(tokens[3]);
                int group = Integer.parseInt(tokens[4]);
                insertWord(db, infinitive, past_Simple, past_Participle, level, group, 0);
            }
        } catch (IOException e) {
            Log.wtf("IrrVerbsDatabaseHelper", "Error reading data file on line " + line, e);
            e.printStackTrace();
        }
    }

    private void insertWord(SQLiteDatabase db, String infinitive, String past_Simple,
                            String past_Participle, int level, int group, int status) {
        ContentValues wordValues = new ContentValues();
        wordValues.put("INFINITIVE", infinitive);
        wordValues.put("PAST_SIMPLE", past_Simple);
        wordValues.put("PAST_PARTICIPLE", past_Participle);
        wordValues.put("LEVEL", level);
        wordValues.put("VERB_GROUP", group);
        wordValues.put("STATUS", status);
        db.insert("TEST", null, wordValues);
    }
}
