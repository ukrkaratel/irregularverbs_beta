package com.karatel.demo.irregularverbsbeta.chooseExercise;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment;

import java.util.*;

import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.SETUP_PIE_CHART;


public class PieChartFragment extends Fragment implements TasksManagerFragment.TaskCallbacks {
    private PieChart pieChart;
    private String[] pieChartNames;
    private int[] pieChartParams;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.fragment_pie_chart, container, false);
        adjustPieChart(parentView);
        if (savedInstanceState != null) {
            pieChartNames = savedInstanceState.getStringArray("pie_chart_names");
            pieChartParams = savedInstanceState.getIntArray("pie_chart_params");
            if (pieChartParams.length == 0)
                ((TasksManagerFragment) getChildFragmentManager()
                        .findFragmentByTag(SETUP_PIE_CHART)).callback(this);
            else
                fillPieChartData();
        } else
            setupPieChart();
        return parentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray("pie_chart_names", pieChartNames);
        outState.putIntArray("pie_chart_params", pieChartParams);
    }

    private void adjustPieChart(View view) {
        pieChart = view.findViewById(R.id.chart_results);
        pieChart.getDescription().setEnabled(false);
        pieChart.setRotationEnabled(true);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDrawEntryLabels(false);
        pieChart.setHoleRadius(0);
        pieChart.animateY(3000);
        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setTextSize(20);
        legend.setTextColor(Color.WHITE);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        pieChart.setNoDataText(null);
        pieChart.setOnChartValueSelectedListener(
                new OnChartValueSelectedListener() {
                    @Override
                    public void onValueSelected(Entry e, Highlight h) {
                        String highlightStr = h.toString();
                        int pos = highlightStr.indexOf("x: ");
                        int num = highlightStr.indexOf("y: ");
                        pos = (int) Float.parseFloat(highlightStr.substring(pos + 3, num - 2));
                        String category = pieChartNames[pos];
                        pos = highlightStr.indexOf(", data");
                        num = (int) Float.parseFloat(highlightStr.substring(num + 3, pos));
                        Toast.makeText(getActivity(), category + ": " + num,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNothingSelected() {

                    }
                }
        );
    }

    private void setupPieChart() {
        TasksManagerFragment.prepare().task(SETUP_PIE_CHART)
                .callback(this).startTask(getChildFragmentManager());
        pieChartNames = getResources().getStringArray(R.array.pie_chart_names);
        pieChartParams = new int[0];
    }

    @Override
    public void onPostExecute(Object result) {
        pieChartParams = (int[]) result;
        fillPieChartData();
    }

    private void fillPieChartData() {
        List<PieEntry> pieEntries = new ArrayList<>();
        for (int i = 0; i < pieChartNames.length; i++)
            pieEntries.add(new PieEntry(pieChartParams[i], pieChartNames[i]));
        PieDataSet dataSet = new PieDataSet(pieEntries, null);
        dataSet.setColors(new int[]{R.color.red,
                R.color.yellow, R.color.green1}, getActivity());
        if (getActivity().getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_PORTRAIT) {
            dataSet.setValueTextSize(20);
            dataSet.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex,
                                                ViewPortHandler viewPortHandler) {
                    return value < 8 ? "" : (int) value + "";
                }
            });
        } else
            dataSet.setDrawValues(false);
        PieData data = new PieData(dataSet);
        pieChart.setData(data);
        pieChart.invalidate();
    }

}
