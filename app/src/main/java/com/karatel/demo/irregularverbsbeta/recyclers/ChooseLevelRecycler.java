package com.karatel.demo.irregularverbsbeta.recyclers;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment;
import com.karatel.demo.irregularverbsbeta.studyActivities.ExerciseActivity;

import java.util.Random;

import static com.karatel.demo.irregularverbsbeta.R.color.*;
import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.GET_LEVELS_TITLES;


public class ChooseLevelRecycler extends ChooseGroupRecycler
        implements TasksManagerFragment.TaskCallbacks {
    private static final String VERBS_GROUP = "VERBS_GROUP";

    private static int[] lightColors;
    private static int[] darkColors;
    private static Random rand;

    static {
        darkColors = new int[]{red, black, indigo, indigo_dark, violet, deep_purple,
                deep_orange, brown, teal};
        lightColors = new int[]{orange, yellow, green1, light_blue, white, amber, lime,
                light_green, cyan, grey};
        rand = new Random();
    }

    private RecyclerView.Adapter<ChooseLevelRecyclerAdapter.ViewHolder> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (savedInstanceState != null)
            fillData();
        return view;
    }

    @Override
    RecyclerView.Adapter<ChooseGroupRecyclerAdapter.ViewHolder> createAdapter() {
        adapter = new ChooseLevelRecyclerAdapter();
        return adapter;
    }

    @Override
    void fillData() {
        if (getTitles().length == 0) {
            Fragment taskFragment = getChildFragmentManager().findFragmentByTag(GET_LEVELS_TITLES);
            if (taskFragment == null) {
                int groupNum = getArguments().getInt(VERBS_GROUP);
                TasksManagerFragment.prepare().task(GET_LEVELS_TITLES).param(groupNum).
                        callback(this).startTask(getChildFragmentManager());
            } else
                ((TasksManagerFragment) taskFragment).callback(this);
        }
    }

    @Override
    public void onPostExecute(Object result) {
        setTitles((String[]) result);
        adapter.notifyDataSetChanged();
        TasksManagerFragment.finishTask(getChildFragmentManager(), GET_LEVELS_TITLES);
    }

    @Override
    RecyclerView.LayoutManager getLayoutManager() {
        return new StaggeredGridLayoutManager(2, 1);
    }

    private class ChooseLevelRecyclerAdapter extends ChooseGroupRecyclerAdapter {

        @Override
        ViewHolder buildHolder(CardView cv) {
            ViewGroup.LayoutParams params = cv.getLayoutParams();
            params.height = params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            return super.buildHolder(cv);
        }

        @Override
        void setupTitle(TextView textView, int position) {
            textView.setTextSize(20);
            textView.setTextColor(getRandomColor(darkColors));
            textView.setBackgroundColor(getRandomColor(lightColors));
            super.setupTitle(textView, position);
        }

        private int getRandomColor(int[] colorsArray) {
            return ContextCompat.getColor(getActivity(),
                    colorsArray[rand.nextInt(colorsArray.length)]);
        }

        @Override
        String getTitle(int position) {
            String str = super.getTitle(position);
            return str.substring(1, str.length());
        }

        @Override
        View.OnClickListener getOnClick(int position) {
            position = super.getTitle(position).charAt(0);
            return new LevelsOnClick(position);
        }

        private class LevelsOnClick implements View.OnClickListener {
            private final int level;

            LevelsOnClick(int level) {
                this.level = level;
            }

            @Override
            public void onClick(View view) {
                if (level == 1)
                    startActivity(ExerciseActivity.
                            makeIntent(getActivity().getApplicationContext(), level));
                else
                    Toast.makeText(getActivity(), "Level " + level + " isn't realize yet",
                            Toast.LENGTH_SHORT).show();
            }
        }
    }

    static ChooseLevelRecycler newInstance(int group) {
        ChooseLevelRecycler recycler = new ChooseLevelRecycler();
        Bundle args = new Bundle();
        args.putInt(VERBS_GROUP, group);
        recycler.setArguments(args);
        return recycler;
    }
}
