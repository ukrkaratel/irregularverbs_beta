package com.karatel.demo.irregularverbsbeta.exercise;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karatel.demo.irregularverbsbeta.R;

public class IncorrectAnswerFragment extends Fragment {
    private static final String INCORRECT_VAR = "incorrect_variant";
    private static final String CORRECT_VAR = "correct_variant";

    private String incorrectStr;
    private String correctStr;

    public static IncorrectAnswerFragment newInstance(String incorrectStr, String correctStr) {
        IncorrectAnswerFragment fragment = new IncorrectAnswerFragment();
        Bundle args = new Bundle();
        args.putString(INCORRECT_VAR, incorrectStr);
        args.putString(CORRECT_VAR, correctStr);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            incorrectStr = getArguments().getString(INCORRECT_VAR);
            correctStr = getArguments().getString(CORRECT_VAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_incorrect_answer, container, false);
        TextView incorrectTv = layout.findViewById(R.id.incorrect_var_txt);
        incorrectTv.setText(incorrectStr);
        incorrectTv.setPaintFlags(incorrectTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        ((TextView) layout.findViewById(R.id.correct_var_txt)).setText(correctStr);
        return layout;
    }

}
