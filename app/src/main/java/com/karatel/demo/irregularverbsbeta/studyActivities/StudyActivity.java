package com.karatel.demo.irregularverbsbeta.studyActivities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.*;
import com.karatel.demo.irregularverbsbeta.exercise.FinishExerciseFragment;

import java.util.*;

import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.*;


abstract class StudyActivity extends AppCompatActivity
        implements CanExercise, TasksManagerFragment.TaskCallbacks {
    private ArrayList<Verb> verbsList = new ArrayList<>();
    private Refreshable currentFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container_layout);
    }

    public void setCurrentFragment(Refreshable currentFragment) {
        this.currentFragment = currentFragment;
    }

    @Override
    public Verb getVerb(int key) {
        return verbsList.get(key);
    }

    @Override
    public int getVerbsListSize() {
        return verbsList.size();
    }

    @Override
    public ArrayList<Verb> getVerbsList() {
        return verbsList;
    }

    @Override
    public boolean removeVerb(Verb verb) {
        verbsList.remove(verb);
        return verbsList.isEmpty();
    }

    @Override
    public void saveAndShowProgress(ArrayMap<String, Integer> verbsStatus) {
        String[] verbsStatusArray = new String[verbsStatus.size()];
        Iterator<Map.Entry<String, Integer>> iterator = verbsStatus.entrySet().iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            Map.Entry<String, Integer> entry = iterator.next();
            verbsStatusArray[i] = entry.getKey() + " " + entry.getValue();
        }
        TasksManagerFragment.prepare().task(SAVE_VERBS_STATUS).param(verbsStatusArray)
                .startTask(getSupportFragmentManager());
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_container, new FinishExerciseFragment(),
                        "current_fragment").
                commit();
    }

    void reestablishProcess(Bundle savedInstanceState) {
        ArrayList<String> savedVerbsList =
                savedInstanceState.getStringArrayList("saved_verbs_list");
        if (!savedVerbsList.isEmpty())
            TasksManagerFragment.prepare().task(GET_VERBS_BY_INFINITIVES).
                    param(savedVerbsList.toArray(new String[savedVerbsList.size()])).
                    startTask(getSupportFragmentManager());
        Fragment lastFragment =
                getSupportFragmentManager().findFragmentByTag("current_fragment");
        if (lastFragment instanceof Refreshable)
            setCurrentFragment((Refreshable) lastFragment);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPostExecute(Object result) {
        verbsList = (ArrayList<Verb>) result;
        if (currentFragment != null)
            currentFragment.refresh();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("saved_verbs_list", saveVerbsList());
    }

    private ArrayList<String> saveVerbsList() {
        ArrayList<String> savedVerbsList = new ArrayList<>();
        for (Verb verb : verbsList)
            savedVerbsList.add(verb.getInfinitive());
        return savedVerbsList;

    }
}
