package com.karatel.demo.irregularverbsbeta.recyclers;

import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.TextView;

import com.karatel.demo.irregularverbsbeta.R;

public class ChooseGroupRecycler extends Fragment {
    private String[] titles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView verbsRecycler = (RecyclerView) inflater.
                inflate(R.layout.fragment_recycler, container, false);
        titles = new String[0];
        verbsRecycler.setAdapter(createAdapter());
        if (savedInstanceState == null)
            fillData();
        else
            titles = savedInstanceState.getStringArray("titles");
        verbsRecycler.setLayoutManager(getLayoutManager());
        return verbsRecycler;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray("titles", titles);
    }

    RecyclerView.Adapter<ChooseGroupRecyclerAdapter.ViewHolder> createAdapter() {
        return new ChooseGroupRecyclerAdapter();
    }

    void setTitles(String[] titles) {
        this.titles = titles;
    }

    String[] getTitles() {
        return titles;
    }

    void fillData() {
        String[] verbsTypesTemp = getResources().getStringArray(R.array.drawer_options);
        String[] titlesTemp = new String[3];
        System.arraycopy(verbsTypesTemp, 2, titlesTemp, 0, 3);
        setTitles(titlesTemp);
    }

    RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    class ChooseGroupRecyclerAdapter
            extends RecyclerView.Adapter<ChooseGroupRecyclerAdapter.ViewHolder> {

        class ViewHolder extends RecyclerView.ViewHolder {
            private CardView cardView;

            ViewHolder(CardView cardView) {
                super(cardView);
                this.cardView = cardView;
            }
        }

        @Override
        public ChooseGroupRecyclerAdapter.ViewHolder onCreateViewHolder(
                ViewGroup parent, int viewType) {
            CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_verbs_type, parent, false);
            return buildHolder(cv);
        }

        ChooseGroupRecyclerAdapter.ViewHolder buildHolder(CardView cv) {
            return new ChooseGroupRecyclerAdapter.ViewHolder(cv);
        }

        @Override
        public void onBindViewHolder(ChooseGroupRecyclerAdapter.ViewHolder holder, int position) {
            CardView cardView = holder.cardView;
            TextView textView = cardView.findViewById(R.id.txt_card_verbs_name);
            setupTitle(textView, position);
            setupOnClick(cardView, position);
        }

        void setupTitle(TextView textView, int position) {
            textView.setText(getTitle(position));
        }

        String getTitle(int position) {
            return titles[position];
        }

        void setupOnClick(CardView cardView, int position) {
            cardView.setOnClickListener(getOnClick(position));
        }

        View.OnClickListener getOnClick(int position) {
            return new ChooseOnClick(position);
        }

        private class ChooseOnClick implements View.OnClickListener {
            private final int groupNum;

            ChooseOnClick(int groupNum) {
                this.groupNum = groupNum;
            }

            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container,
                                ChooseLevelRecycler.newInstance(groupNum))
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        }

        @Override
        public int getItemCount() {
            return titles.length;
        }

    }
}
