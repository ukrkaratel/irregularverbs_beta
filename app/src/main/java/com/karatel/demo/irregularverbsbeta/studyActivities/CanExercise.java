package com.karatel.demo.irregularverbsbeta.studyActivities;

import android.support.v4.util.ArrayMap;

import com.karatel.demo.irregularverbsbeta.data.Verb;

import java.util.ArrayList;

public interface CanExercise {
    Verb getVerb(int key);

    ArrayList<Verb> getVerbsList();

    int getVerbsListSize();

    boolean removeVerb(Verb verb);

    void saveAndShowProgress(ArrayMap<String, Integer> verbsStatus);
}