package com.karatel.demo.irregularverbsbeta.demonstration;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.Verb;
import com.karatel.demo.irregularverbsbeta.exercise.ExerciseFragment;
import com.karatel.demo.irregularverbsbeta.studyActivities.*;
import com.karatel.demo.irregularverbsbeta.tables.TablesActivity;


public class FinishDemonstrationFragment extends Fragment implements Refreshable {
    private CanExercise canExercise;
    private View layout;
    private ArrayAdapter<Verb> adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CanExercise))
            throw new RuntimeException(context.toString() +
                    " must implemented CanGiveVerbsList");
        else
            canExercise = (CanExercise) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_finish_demonstration, container, false);
        setupButtonsClicks();
        setupListView();
        return layout;
    }

    private void setupButtonsClicks() {
        layout.findViewById(R.id.start_exercise_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                new ExerciseFragment(),
                                "current_fragment").
                        commit();
            }
        });
        layout.findViewById(R.id.look_again_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToVerb(0);
            }
        });
    }

    private void setupListView() {
        ListView listView = layout.findViewById(R.id.demo_list);
        adapter = new TablesActivity.TablesListAdapter(getActivity(), canExercise.getVerbsList());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                goToVerb(position);
            }
        });
    }

    private void goToVerb(int position) {
        DemonstrationFragment demoFragment =
                position == 0
                        ? new DemonstrationFragment()
                        : DemonstrationFragment.newInstance(position);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, demoFragment, "current_fragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void refresh() {
        if (getView() == null) return;
        adapter.addAll(canExercise.getVerbsList());
    }
}
