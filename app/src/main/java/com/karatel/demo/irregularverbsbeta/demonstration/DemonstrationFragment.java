package com.karatel.demo.irregularverbsbeta.demonstration;


import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.Verb;
import com.karatel.demo.irregularverbsbeta.exercise.ExerciseFragment;
import com.karatel.demo.irregularverbsbeta.studyActivities.CanExercise;
import com.karatel.demo.irregularverbsbeta.studyActivities.Refreshable;

import java.io.IOException;

public class DemonstrationFragment extends Fragment implements Refreshable {
    private static final String VERB_NUM = "VERB_NUM";

    private CanExercise canExercise;
    private MediaPlayer mediaPlayer;
    private ImageView verbs_img;
    private TextView tvForms;
    private TextView tvTranslation;
    private String packageName;
    private int currentPosition;
    private int limit = -1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CanExercise))
            throw new RuntimeException(context.toString() +
                    " must implemented CanExercise");
        else
            canExercise = (CanExercise) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_demonstration, container, false);
        verbs_img = layout.findViewById(R.id.verb_image);
        tvForms = layout.findViewById(R.id.txt_forms);
        tvTranslation = layout.findViewById(R.id.txt_translation);
        mediaPlayer = new MediaPlayer();
        ImageButton btn_next_verb = layout.findViewById(R.id.btn_next_verb);
        ImageButton btn_previous_verb = layout.findViewById(R.id.btn_previous_verb);
        ImageButton btn_listen_to = layout.findViewById(R.id.btn_listen_to);
        packageName = getActivity().getPackageName();
        if (getArguments() != null)
            currentPosition = getArguments().getInt(VERB_NUM);
        if (savedInstanceState != null)
            currentPosition = savedInstanceState.getInt("currentPosition");
        if (canExercise.getVerbsListSize() != 0)
            refresh();
        btn_next_verb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limit == -1) return;
                if (++currentPosition == limit) {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new FinishDemonstrationFragment(),
                                    "current_fragment")
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit();
                } else
                    changeVerb(canExercise.getVerb(currentPosition));
            }
        });
        btn_previous_verb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limit == -1) return;
                if (--currentPosition < 0)
                    currentPosition = limit - 1;
                changeVerb(canExercise.getVerb(currentPosition));
            }
        });
        btn_listen_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limit == -1) return;
                mediaPlayer.start();
            }
        });
        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_demonstration, menu);
        MenuItem item = menu.getItem(0);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                getFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                new ExerciseFragment(),
                                "current_fragment").
                        commit();
                return false;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentPosition", currentPosition);
    }

    public void refresh() {
        if (verbs_img == null) return;
        changeVerb(canExercise.getVerb(currentPosition));
        limit = canExercise.getVerbsListSize();
    }

    public void changeVerb(Verb verb) {
        verbs_img.setImageResource(getResources().getIdentifier(verb.getInfinitive() +
                "_img", "drawable", packageName));
        verbs_img.setContentDescription(verb.getTranslation());
        boolean isPortrait = getActivity().getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_PORTRAIT;
        setFormsText(verb, isPortrait);
        setTranslationText(verb.getTranslation(), isPortrait);
        setAudio(verb.getInfinitive());
    }


    private void setFormsText(Verb verb, boolean isPortrait) {
        String verbs_forms = verb.getInfinitive() + " - " + verb.getPastSimple() + " - " +
                verb.getPastParticiple();
        if (verbs_forms.length() < 16)
            tvForms.setTextSize(isPortrait ? 50 : 35);
        else if (verbs_forms.length() < 22)
            tvForms.setTextSize(isPortrait ? 35 : 30);
        else if (verbs_forms.length() < 60)
            tvForms.setTextSize(isPortrait ? 28 : 25);
        tvForms.setText(verbs_forms);
    }

    private void setTranslationText(String translation, boolean isPortrait) {
        if (translation.length() < 31)
            tvTranslation.setTextSize(isPortrait ? 35 : 25);
        else if (translation.length() < 45)
            tvTranslation.setTextSize(isPortrait ? 25 : 25);
        tvTranslation.setText(translation);
    }

    private void setAudio(String infinitive) {
        int audioId = getResources().getIdentifier(infinitive + "_audio", "raw",
                packageName);
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(getActivity(),
                    Uri.parse("android.resource://com.karatel.demo.irregularverbsbeta/" + audioId));
            mediaPlayer.prepare();
        } catch (IOException e) {
            mediaPlayer = MediaPlayer.create(getActivity(), audioId);
        }

    }

    public static DemonstrationFragment newInstance(int verbNum) {
        DemonstrationFragment demoFragment = new DemonstrationFragment();
        Bundle args = new Bundle();
        args.putInt(VERB_NUM, verbNum);
        demoFragment.setArguments(args);
        return demoFragment;
    }
}
