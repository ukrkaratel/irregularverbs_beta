package com.karatel.demo.irregularverbsbeta.studyActivities;

import android.os.Bundle;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.chooseExercise.ExerciseTopFragment;

public class RepeatActivity extends StudyActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().
                    add(R.id.fragment_container, new ExerciseTopFragment(), "current_fragment").
                    commit();
        } else
            reestablishProcess(savedInstanceState);
    }
}
