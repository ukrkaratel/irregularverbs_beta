package com.karatel.demo.irregularverbsbeta.studyActivities;

import android.content.*;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment;
import com.karatel.demo.irregularverbsbeta.demonstration.DemonstrationFragment;

import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.GET_VERBS_BY_LEVEL;

public class ExerciseActivity extends StudyActivity {
    private static final String LEVEL = "LEVEL";
    private int level;

    public static Intent makeIntent(Context packageContext, int level){
        Intent intent = new Intent(packageContext, ExerciseActivity.class);
        intent.putExtra(LEVEL, level);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            level = getIntent().getExtras().getInt(LEVEL);
            TasksManagerFragment.prepare().task(GET_VERBS_BY_LEVEL).
                    param(level).startTask(getSupportFragmentManager());
            DemonstrationFragment demonstrationFragment = new DemonstrationFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, demonstrationFragment, "current_fragment")
                    .commit();
            setCurrentFragment(demonstrationFragment);
        } else {
            reestablishProcess(savedInstanceState);
            level = savedInstanceState.getInt(LEVEL);
        }
    }

    @Override
    public void saveAndShowProgress(ArrayMap<String, Integer> verbsStatus) {
        super.saveAndShowProgress(verbsStatus);
        if (updateLevel())
            updateLearnedVerbsNum(verbsStatus.size());
    }

    private boolean updateLevel() {
        SharedPreferences sharedPrefs = getSharedPreferences("open_levels", Context.MODE_PRIVATE);
        String groupType;
        int firstLevel;
        if (level <= 5) {
            groupType = "unchangeable";
            firstLevel = 1;
        } else if (level <= 27) {
            groupType = "two_forms_same";
            firstLevel = 6;
        } else {
            groupType = "all_forms_different";
            firstLevel = 29;
        }
        if (level == sharedPrefs.getInt(groupType, firstLevel)) {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt(groupType, ++level);
            editor.apply();
            return true;
        } else
            return false;
    }

    private void updateLearnedVerbsNum(int learnedVerbsNumber) {
        SharedPreferences sharedPrefs =
                getSharedPreferences("users_progress", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt("learned_verbs_number", learnedVerbsNumber);
        editor.apply();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LEVEL, level);
    }
}

