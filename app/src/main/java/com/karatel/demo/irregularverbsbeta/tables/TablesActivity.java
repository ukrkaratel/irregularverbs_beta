package com.karatel.demo.irregularverbsbeta.tables;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.*;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.karatel.demo.irregularverbsbeta.data.*;
import com.karatel.demo.irregularverbsbeta.R;

import java.util.ArrayList;

import static com.karatel.demo.irregularverbsbeta.data.TasksManagerFragment.GET_ALL_VERBS;

public class TablesActivity extends AppCompatActivity
        implements TasksManagerFragment.TaskCallbacks,
        SearchView.OnQueryTextListener,
        ListView.OnItemClickListener {

    private String[] drawerOptions;
    private ListView drawerList;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int currentPosition;
    private ArrayList<Verb> verbsList;
    private ArrayAdapter<Verb> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tables);
        if (savedInstanceState != null) {
            Fragment taskFragment = getSupportFragmentManager().findFragmentByTag(GET_ALL_VERBS);
            if (taskFragment == null)
                receiveData();
            currentPosition = savedInstanceState.getInt("position");
        } else {
            receiveData();
            currentPosition = 1;
        }
        verbsList = new ArrayList<>();
        ListView tableLV = (ListView) findViewById(R.id.table_list_view);
        adapter = new TablesListAdapter(TablesActivity.this, new ArrayList<>(verbsList));
        tableLV.setAdapter(adapter);
        drawerOptions = getResources().getStringArray(R.array.drawer_options);
        drawerList = (ListView) findViewById(R.id.drawer_list);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_activated_1, drawerOptions));
        drawerList.setOnItemClickListener(this);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchView sv = (SearchView) menu.findItem(R.id.search_view).getActionView();
        sv.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", currentPosition);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onPostExecute(Object result) {
        verbsList = (ArrayList<Verb>) result;
        selectItem(currentPosition);
        TasksManagerFragment.finishTask(getSupportFragmentManager(), GET_ALL_VERBS);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        selectItem(position);
    }

    private void receiveData() {
        TasksManagerFragment.prepare().task(GET_ALL_VERBS).startTask(getSupportFragmentManager());
    }

    private void selectItem(int position) {
        currentPosition = position;
        if (position == 0)
            finish();
        else {
            adapter.clear();
            ArrayList<Verb> currentList;
            if (position > 1) {
                int groupNum = position - 1;
                currentList = filtrate(verbsList, Selection.forVerbsGroup(groupNum));
            } else
                currentList = new ArrayList<>(verbsList);
            adapter.addAll(currentList);
            setActionBarTitle(position);
            drawerList.setItemChecked(position, true);
            drawerLayout.closeDrawer(drawerList);
        }
    }

    private void setActionBarTitle(int position) {
        getSupportActionBar().setTitle(drawerOptions[position]);
    }

    private static ArrayList<Verb> filtrate(ArrayList<Verb> list, Predicate<Verb> selection) {
        return new ArrayList<>(Collections2.filter(list, selection));
    }

    public static class TablesListAdapter extends ArrayAdapter<Verb> {
        private Activity parentActivity;
        private ArrayList<Verb> verbsList;
        private View convertView;

        public TablesListAdapter(Activity context, ArrayList<Verb> verbsList) {
            super(context, R.layout.row_in_table, verbsList);
            parentActivity = context;
            this.verbsList = verbsList;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null)
                convertView = parentActivity.getLayoutInflater()
                        .inflate(R.layout.row_in_table, parent, false);
            this.convertView = convertView;

            Verb currentVerb = getItem(position);
            setTextAndBackground(R.id.table_txt_infinitive, currentVerb.getInfinitive(),
                    R.color.orange);
            setTextAndBackground(R.id.table_txt_past_simple, currentVerb.getPastSimple(),
                    R.color.yellow);
            setTextAndBackground(R.id.table_txt_participle, currentVerb.getPastParticiple(),
                    R.color.green1);
            setTextAndBackground(R.id.table_txt_translation, currentVerb.getTranslation(),
                    R.color.light_blue);
            return convertView;
        }

        private void setTextAndBackground(@IdRes int textViewId, String text,
                                          @ColorRes int colorId) {
            TextView textView = convertView.findViewById(textViewId);
            textView.setText(text);
            textView.setBackground(getSelector(colorId));
        }

        private StateListDrawable getSelector(@ColorRes int colorId) {
            int pressedColor = ContextCompat.getColor(convertView.getContext(), R.color.red);
            int tvColor = ContextCompat.getColor(convertView.getContext(), colorId);
            StateListDrawable selector = new StateListDrawable();
            selector.addState(new int[]{android.R.attr.state_pressed},
                    new ColorDrawable(pressedColor));
            selector.addState(new int[]{}, new ColorDrawable(tvColor));
            return selector;
        }

        @NonNull
        @Override
        public Filter getFilter() {
            return new VerbFilter();
        }

        private class VerbFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null && constraint.length() > 0) {
                    String searchVerb = constraint.toString().toLowerCase();
                    ArrayList<Verb> filterList =
                            filtrate(verbsList, Selection.forSearch(searchVerb));
                    results.count = filterList.size();
                    results.values = filterList;
                } else {
                    results.count = verbsList.size();
                    results.values = new ArrayList<>(verbsList);
                }
                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                TablesListAdapter.this.clear();
                TablesListAdapter.this.addAll((ArrayList<Verb>) results.values);
            }
        }
    }
}