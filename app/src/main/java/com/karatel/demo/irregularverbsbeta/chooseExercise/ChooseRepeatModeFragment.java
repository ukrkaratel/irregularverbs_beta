package com.karatel.demo.irregularverbsbeta.chooseExercise;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.*;
import android.widget.*;

import com.karatel.demo.irregularverbsbeta.R;
import com.karatel.demo.irregularverbsbeta.data.Verb;
import com.karatel.demo.irregularverbsbeta.exercise.ExerciseFragment;
import com.karatel.demo.irregularverbsbeta.studyActivities.CanExercise;

import java.util.*;


public class ChooseRepeatModeFragment extends Fragment {
    private CanExercise canExercise;
    private LinearLayout fieldsLayout;
    private AppCompatRadioButton[] radioGroup;
    private int checkedBtnInd;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CanExercise))
            throw new RuntimeException(context.toString()
                    + " must implement CanExercise");
        else
            canExercise = (CanExercise) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View parent = inflater.inflate(R.layout.fragment_choose_repeat_mode, container, false);
        fieldsLayout = parent.findViewById(R.id.fields_layout);
        FloatingActionButton startExerciseBtn = parent.findViewById(R.id.start_exercise_btn);
        startExerciseBtn.setOnClickListener(new StartExerciseOnClick());
        int learnedVerbsNum = parent.getContext().
                getSharedPreferences("users_progress", Context.MODE_PRIVATE).
                getInt("learned_verbs_number", 0);
        if (learnedVerbsNum > 5) {
            addField(learnedVerbsNum);
            if (savedInstanceState != null)
                checkedBtnInd = savedInstanceState.getInt("checked_buttons_index");
        } else
            ((AppCompatRadioButton) parent.findViewById(R.id.all_verbs_btn)).setChecked(true);
        return parent;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("checked_buttons_index", checkedBtnInd);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        canExercise = null;
    }

    private class StartExerciseOnClick implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (canExercise.getVerbsListSize() == 0) {
                Toast.makeText(getActivity(), getString(R.string.no_verbs),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (checkedBtnInd == 1) {
                int verbsAmount = Integer.parseInt(
                        String.valueOf(
                                ((Spinner) fieldsLayout.
                                        findViewById(R.id.repeat_spinner)).
                                        getSelectedItem()));
                List<Verb> newList = new ArrayList<>();
                Collections.shuffle(canExercise.getVerbsList());
                for (int i = 0; i < verbsAmount; i++)
                    newList.add(canExercise.getVerb(i));
                canExercise.getVerbsList().retainAll(newList);
            }
            getActivity().getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragment_container, new ExerciseFragment(), "current_fragment").
                    commit();
        }
    }

    private void addField(int learnedVerbsNum) {
        initRadioGroup();
        if (learnedVerbsNum > 50)
            learnedVerbsNum = 50;
        createField(learnedVerbsNum);
    }

    private void initRadioGroup() {
        radioGroup = new AppCompatRadioButton[2];
        AppCompatRadioButton allVerbsBtn = fieldsLayout.findViewById(R.id.all_verbs_btn);
        allVerbsBtn.setOnClickListener(new RadioButtonOnClick(0));
        allVerbsBtn.setChecked(true);
        radioGroup[0] = allVerbsBtn;
    }

    private void createField(int learnedVerbsNum) {
        View newField = LayoutInflater.from(fieldsLayout.getContext()).
                inflate(R.layout.choose_repeat_mode_field, fieldsLayout, false);
        Spinner spinner = newField.findViewById(R.id.repeat_spinner);
        ArrayList<String> spinnersItems = new ArrayList<>();
        for (int item = 5; item <= learnedVerbsNum; item += 5)
            spinnersItems.add(Integer.toString(item));
        spinner.setAdapter(new ArrayAdapter<>(fieldsLayout.getContext(),
                R.layout.spinner_item,
                spinnersItems));
        AppCompatRadioButton radioButton = newField.findViewById(R.id.repeat_radio_btn);
        radioButton.setOnClickListener(new RadioButtonOnClick(1));
        radioGroup[1] = radioButton;
        fieldsLayout.addView(newField);
    }

    private class RadioButtonOnClick implements View.OnClickListener {
        private int position;

        private RadioButtonOnClick(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (checkedBtnInd == position) return;
            radioGroup[checkedBtnInd].setChecked(false);
            checkedBtnInd = position;
        }
    }

}
