package com.karatel.demo.irregularverbsbeta.studyActivities;

public interface Refreshable {
    void refresh();
}
