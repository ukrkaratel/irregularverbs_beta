package com.karatel.demo.irregularverbsbeta.tables;

import com.google.common.base.Predicate;
import com.karatel.demo.irregularverbsbeta.data.Verb;

class Selection {
    static Predicate<Verb> forVerbsGroup(final int groupNum) {
        return new Predicate<Verb>() {
            @Override
            public boolean apply(Verb verb) {
                return verb.getGroupNum() == groupNum;
            }
        };
    }

    static Predicate<Verb> forSearch(final String searchVerb) {
        return new Predicate<Verb>() {
            @Override
            public boolean apply(Verb verb) {
                for (String field : new String[]{verb.getInfinitive(),
                        verb.getPastSimple(), verb.getPastParticiple(),
                        verb.getTranslation()})
                    if (field.contains(searchVerb))
                        return true;
                return false;
            }
        };
    }

}
